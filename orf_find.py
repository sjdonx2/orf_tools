#!/usr/bin/env python2
""" Find ORF IDs and gene names

Intersect a w/ or w/o hydroxyurea bed file with annotation data and parse
out the ORF IDs and gene names in the resulting bed file
"""

# TODO: Possibly eliminate use of csv, since the rows are built manually
import csv
import urllib
import argparse
import pybedtools as bt
import utils

# TODO: Build rows as one string instead of a list of one or more strings
def parse_orfs(data):
    """ Parse out ORF IDs and gene names from annotation data (BedTool) and
    yield a list containing all valid IDs.

    At a minimum, the feature must have the name of SGD ID and optionally the
    gene name associated with the SGD ID. Data is the resulting BedTool from
    intersecting the annotation and experimental data.

    Skipped data includes:
        features that do not have ID names
        features with chromosomes as IDs, eg "chrIV"
        mRNA versions of features, eg "$SGD_ID"_mRNA
        Telomeric features, eg TEL04R, TEL04R_X_element_combinatoial_repeat,
            TEL04R_Y_prime_element,TEL04R_telomeric_repeat
    """
    genecnt = 0
    written = set()
    for feature in data:
        attributes = feature.attrs
        feat_type = feature.fields[2]
        row = []

        if feat_type == 'gene' or feat_type == 'tRNA_gene':
            if 'ID' in attributes:

                # tRNA identifiers have percent encoded parens, unquote is used to
                # convert them back to parens
                orf_id = urllib.unquote(attributes['ID']).upper()

                if orf_id not in written:
                    written.add(orf_id)
                    row.append(orf_id)
                    if 'gene' in attributes:
                        row.append(attributes['gene'])
                    genecnt += 1
                    yield ",".join(row)
    print("Mark count: %d" % genecnt)

def annotate_bed(bed_file):
    """ Find overlaps of the ORF promoter region (defined as 300bp upstream of
    ORF) and binding sites of found in the h3t45 experiments
    """

    sacc_gff = utils.in_dir("saccharomyces_cerevisiae.gff")
    chrom_info = utils.in_dir("ChromInfo.txt")

    bed = bt.BedTool(bed_file)
    annotation = bt.BedTool(sacc_gff)
    upstream = annotation.flank(g=chrom_info, l=300, r=0, s=True)
    bed_annotated = upstream.intersect(bed, wa=True, wb=True)

    return bed_annotated

def write_parsed(data, writer):
    """ Writes parsed orfs from parse_orfs to a csv file"""
    for row in parse_orfs(data):
        # TODO: Investigate why [row] for rows that had two ids keeps
        # the quotes
        writer.writerow(row.split(","))

if __name__ == '__main__':
    CMD_LINE = argparse.ArgumentParser(description="""
            Given an list of yeast coordinate data as a bed file, find overlaps
            of those coordinates in the promoter regions of yeast genes, writing
            each genes systematic name and standard name (if it exists)
    """, epilog="*Note: skips *_mRNA and telomeric positions")
    CMD_LINE.add_argument("exp")
    ARGS = CMD_LINE.parse_args()

    EXP_DATA = ARGS.exp
    OUTPUT_PREFIX = EXP_DATA.split(".")[0]
    ANNOTATED = annotate_bed(EXP_DATA)

    with open(OUTPUT_PREFIX + ".csv", "w") as csvfile:
        write_parsed(ANNOTATED, csv.writer(csvfile))
