#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Utlitiy for converting arabic numerals to roman values for chromosome number
"""

import sys
import pybedtools as bt

NUMERIC_DICT = {
    "I": "1",
    "II": "2",
    "III": "3",
    "IV": "4",
    "V": "5",
    "VI": "6",
    "VII": "7",
    "VIII": "8",
    "IX": "9",
    "X": "10",
    "XI": "11",
    "XII": "12",
    "XIII": "13",
    "XIV": "14",
    "XV": "15",
    "XVI": "16",
    "XVII": "17",
    "XVIII": "18",
    "XIX": "19",
    "XX": "20",
}

ARAB_DICT = {v: k for k, v in NUMERIC_DICT.items()}

def convert(chrm, table):
    """ Given a conversion table replaces each character in chrm
    with its corresponding value in dictionary table
    """

    numeric = chrm.lstrip("chr")
    translated = table[numeric] if numeric in table else numeric
    return "".join(["chr", translated])

def to_roman(chrm):
    """ Converts arabic numerals to roman numerals
    """
    return convert(chrm, ARAB_DICT)

def to_arab(chrm):
    """ Converts roman numerals to arabic numerals
    """
    return convert(chrm, NUMERIC_DICT)

def convert_bedfile(bed_file, output_filename):
    """ For each feature in bed_file convert the chromosome
    number to its arabic or roman equivalent and save to
    output_filename
    """
    btool = bt.BedTool(bed_file)
    inters = []
    for feat in btool:
        feat.chrom = to_roman(feat.chrom)
        inters.append(feat)
    new_bt = bt.BedTool(inters)
    new_bt.saveas(output_filename)

if __name__ == "__main__":
    BED_FILE = sys.argv[1]
    OUTPUT_FILE = sys.argv[2]
    convert_bedfile(BED_FILE, OUTPUT_FILE)
