#!/usr/bin/env python2
# -*- coding: utf-8 -*-
""" Count the number of reads in the provided bedgraph that overlap with
the 500bp upstream promoter region of each gene in the list provided by Pugh et al.
and create an ordering based on that. Based on that ordering, possibly order other files.
"""

import sys
import string
import csv
import pybedtools as bt
import utils

# TODO: Allow modify of gene_file name
# TODO: Expand to take in different levels of numbers of genes, etc.
def upstream_filtered(bed_file, prom_size):
    """ Generates a bedtool with each gene's upstream promoter region
    defined by prom_size and filtered, only allowing features that are genes or tRNA_genes
    and are in a gene_list given by h3.txt
    """
    gene_file = utils.in_dir("h3.txt")
    chrom_info = utils.in_dir("ChromInfo.txt")

    gene_list = get_gene_list(gene_file)
    filtered = gene_filter(gene_list)

    prom_region = filtered.flank(g=chrom_info, l=prom_size, r=0, s=True)
    intervals = bt.BedTool(bed_file)
    intersect = prom_region.intersect(intervals, wa=True, wb=True)

    return intersect

def gene_filter(gene_list):
    """ Returns a filtered version of the reference gff file that only contains
    genes and tRNA_genes that are members of gene_list
    """
    sacc_gff = utils.in_dir("saccharomyces_cerevisiae.gff")
    sacc_bt = bt.BedTool(sacc_gff)

    filtered = sacc_bt.filter(lambda i: feat_filter(i, gene_list))

    return filtered

def feat_filter(feat, gene_list):
    """ Criteria that each interval feature is filtered based on.  """

    is_gene = feat.fields[2] == 'gene' or feat.fields[2] == 'tRNA_gene'
    is_desired_gene = feat.name in gene_list

    return is_gene and is_desired_gene

def all_genes(feat):
    return feat.fields[2] == 'gene'

def get_gene_list(filename):
    """ Generates the list of genes that will be used to filter
    the reference gff file
    """
    genes = []
    with open(filename, "r") as gene_list:
        gene_list.next()
        for line in gene_list:
            name = string.split(line, '\t', 1)[0]
            genes.append(name)
    return set(genes)

def overlap_calc(feat):
    """ For a given feature in a bed file, calculates the number of reads that overlap
    with the promoter region of the feature. If there isn't complete overlap, calculate the
    fraction of the read bin that overlaps with the promotor regions and multiply the read value
    by that value.
    ie
    promoter region 0   - 200
    read bin        150 - 300
    read value      34.74
    overlap value   = 34.74 * (50 / 150)
    """
    tss_start = int(feat.start)
    tss_stop = int(feat.stop)
    gene_name = feat.name
    read_start = int(feat.fields[10])
    read_stop = int(feat.fields[11])
    read_value = float(feat.fields[12])

    if read_start < tss_start:
        read_value *= (read_stop - tss_start) / 150.0
    elif read_stop > tss_stop:
        read_value *= (tss_stop - read_start) / 150.0

    return (gene_name, read_value)

def gene_value_dict(intersected):
    """ Given a processed BedTool (using upstream_filtered) generates
    a dictionary containing each gene and its corresponding each count
    """
    value_dict = {}

    for feat in intersected:
        name, val = overlap_calc(feat)
        if name not in value_dict:
            value_dict[name] = val
        else:
            value_dict[name] += val

    return value_dict

def write_dict(vdict):
    """ Given the value dictionary, writes the results to results.csv """

    with open("results.csv", 'w') as fname:
        writer = csv.writer(fname)
        writer.writerow(["Gene", "ReadCnt"])
        writer.writerows(vdict.items())

if __name__ == "__main__":
    BED_FILE = sys.argv[1]
    FILTERED = upstream_filtered(BED_FILE, 500)
    FILTERED.saveas("filtered.bed")
    VALUE_DICT = gene_value_dict(FILTERED)
    write_dict(VALUE_DICT)
