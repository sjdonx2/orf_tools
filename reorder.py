#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
Based on an ordering generated by h3_reads.py,
reorder different file based on that ordering
"""

import sys

def reorder(order_table, h3_lines):
    only_gene_order = [x.split(',')[0] for x in order_table]
    new_order = [get_data(h3_lines, gene) for gene in only_gene_order]

    return new_order

def get_data(h3_lines, gene):
    for h3_line in h3_lines:
        h3_gene = h3_line.split('\t', 1)[0]
        if h3_gene == gene:
            return h3_line
    print("Not found: %s", gene)
    return ""

if __name__ == "__main__":
    ORDERING = sys.argv[1]
    HISTONE = sys.argv[2]
    HIST_PREFIX = HISTONE.rsplit('.', 1)[0]

    with open(ORDERING, 'r') as order_data:
        order_data.readline()
        ORDER_LINES = order_data.readlines()

    with open(HISTONE, 'r') as HIST_DATA:
        HEADER = HIST_DATA.readline()
        HIST_LINES = HIST_DATA.readlines()

    NEW_ORDER = reorder(ORDER_LINES, HIST_LINES)

    with open("new_" + HIST_PREFIX + ".csv", 'w') as wfile:
        wfile.write(HEADER)
        for line in NEW_ORDER:
            wfile.write(line)
