#!/usr/bin/bash

base=$(basename "$1" .csv)
fpart=${base:4}
fname="$fpart"_top_1000.csv

head -1 "$1" > $fname
tail -1000 "$1" >> $fname
cp "$base".jtv "$fpart"_top_1000.jtv

echo "New file is: "
echo $fname
