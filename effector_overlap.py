#!/usr/bin/env python2
""" Find overlaps between effector data and generated data

For each overlap it find between the data, write out the statistic
row in the data to data_overlaps.csv"
"""

import csv
import argparse

def data_overlaps(data, effectors, wfile):
    """ Write overlaps if the effector is in the data file """
    datalist = list(data)
    for effector in effectors:
        for row in datalist:
            if any([gene_id in row for gene_id in effector]):
                wfile.writerow(row)

if __name__ == '__main__':
    CMD_LINE = argparse.ArgumentParser(description="""
            Given an list of yeast coordinate data as a bed file, find overlaps
            of those coordinates in the promoter regions of yeast genes, writing
            each genes statistical information to data_overlaps.csv
    """)
    CMD_LINE.add_argument("data")
    CMD_LINE.add_argument("eff")
    ARGS = CMD_LINE.parse_args()
    EXP_DATA = ARGS.data
    EFFECTORS = ARGS.eff

    with open(EXP_DATA, 'r') as dataf, \
            open(EFFECTORS, 'r') as effectorsf, \
            open("data_overlaps.csv", 'w') as overlapf:
        DATA_READER = csv.reader(dataf)
        EFF_READER = csv.reader(effectorsf)
        OVERLAP_WRITER = csv.writer(overlapf)
        data_overlaps(DATA_READER, EFF_READER, OVERLAP_WRITER)
        print("Overlaps and statistics writtent o data_overlaps.csv")
