#!/usr/bin/env python2
""" Filter out genes that have no known regulations

Part of the orf statistics pipeline, taking data from orf_find and removing
genes that have no known regulations in order to reduce the effect of any
unknown bias that may result from the regulation table and how regulations
are counted
"""

import csv
import argparse
import orf_stats
import utils

def filter_reg(regulations, rfile, wfile):
    """ Takes each row, containing either one or two identfiers and tests
    for membership the list containging the nonredunant list of regulated genes
    from  the regulation table.
    """
    for row in rfile:
        row = row.strip().split(',')
        for gene_id in row:
            if gene_id in regulations:
                wfile.writerow(row)

if __name__ == '__main__':
    CMD_LINE = argparse.ArgumentParser(description="")
    CMD_LINE.add_argument("exp")
    ARGS = CMD_LINE.parse_args()
    TF_TABLE = utils.in_dir("RegulationTable.tsv")

    EXP_DATA = ARGS.exp
    OUTPUT_PREFIX = "Filtered" + EXP_DATA.split(".")[0]
    GENE_LIST = orf_stats.gene_tf_dict(TF_TABLE).keys()
    with open(OUTPUT_PREFIX + ".csv", "w") as csvfile, open(EXP_DATA, "r") as expfile:
        FILTER_FILE = csv.writer(csvfile)
        filter_reg(GENE_LIST, expfile, FILTER_FILE)

    print("Output written to " + OUTPUT_PREFIX + ".csv")
