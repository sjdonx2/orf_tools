#!/usr/bin/env python2
# -*- coding: utf-8 -*-
""" Generates promoter regions of every gene, chopping into 4bp
bin intervals. Given a bed file containing raw reads, overlap
values are calculated for each bin, and smoothed with a 3 bin
moving average. This data is written to a csv file with each orf
having a value for every 4 bp bin in the defined promoter region.
"""

import copy
import csv
import argparse
import multiprocessing
import numpy as np
import pybedtools as bt

import utils
from h3_reads import get_gene_list

def promoters():
    """ Generates a promoter region for each gene, and chops it
    into 4 bp bins."""

    sacc_gff = utils.in_dir("saccharomyces_cerevisiae.gff")
    chrom_info = utils.in_dir("ChromInfo.txt")
    pugh_file = utils.in_dir("h3.txt")

    pugh_genes = get_gene_list(pugh_file)
    sacc_bt = bt.BedTool(sacc_gff)

    # Since promoter regions that extend pass the beginning or end of the chromosome
    # can be generated, the last filter removes those, because the defined promoter
    # is 996 bp long
    prom_region = sacc_bt.filter(lambda feat: feat.name in pugh_genes) \
                         .flank(g=chrom_info, l=398, r=0, s=True) \
                         .slop(g=chrom_info, l=0, r=598, s=True)  \
                         .filter(lambda feat: feat.length == 996)
    return prom_region

# TODO make use of bin_size parameter
def chop_prom(feat_prom):
    """ Separates promoter region of a single feature into 4 bp intervals"""
    start = feat_prom.start
    bins = []
    for bin_pos in range(250):
        new_feat = copy.copy(feat_prom)
        new_feat.start = start + (4 * bin_pos)
        new_feat.stop = new_feat.start + 4
        bins.append(new_feat)

    return bins

def chop_promoters(prom_region):
    """ For each promoter region in prom_region, chop into 4 bp bins"""
    bin_list = [chop_prom(feat) for feat in prom_region]
    bins_bt_list = [bt.BedTool(bins) for bins in bin_list]

    return bins_bt_list

def intersect_bins(bins_bt_list, pool_size):
    "Given a BedTool bins_bt that contains every gene promoter split into 4bp bins"

    pool = multiprocessing.Pool(pool_size)
    # reads_bt = bt.BedTool(reads_bf)
    # intersect_bt_list = [bins_bt.intersect(reads_bt, c=True) for bins_bt in bins_bt_list]

    intersect_bt_list = pool.map(intersection, bins_bt_list)
    pool.close()
    pool.join()

    return intersect_bt_list

def intersection(bin_bt):
    """ lambdas can't be used in multiprocessing Pools """
    reads_bf = ARGS.reads
    reads_bt = bt.BedTool(reads_bf)
    return bin_bt.intersect(reads_bt, c=True, sorted=True)

def collect_gene_bins(intersect_bt_list):
    """ Collect features in overlap_bt into the same genes and a list of their overlap values in a
    dictionary"""
    gene_collection = {}
    for intersect_bt in intersect_bt_list:
        # Last field in the feature is the number of reads overlapped
        scores = [int(feat.fields[-1]) for feat in intersect_bt]
        gene_collection[intersect_bt[0].name] = scores

    return gene_collection

# TODO make use of window_size parameter
def smooth_bins(gene_collection):
    """ Smooth the bin values using a 3 bin moving average, assigning the average value to the
    leftmost bin. The last values are average with the last two, and final bin left unaveraged"""

    # No dictionary comprehensions in python 2.6.6, cb48
    return dict([(gene, bin_smooth(bin_list)) for gene, bin_list in gene_collection.iteritems()])

def three_bin_windows(lst):
    """ Given a list, generates an iterator and returns a list of 3 bin values defined by a sliding
    window. Because the last values don't have enough values, the windows are truncated, with the
    second to last window containing two values, and the last window being one value."""

    yield lst[0:2]

    idx = 1
    while idx + 1 < len(lst):
        yield lst[idx-1:idx+2]
        idx += 1

    yield lst[-2:]

# TODO see if rolling median is better
def bin_smooth(bin_list):
    """ After converting the bins into 3 bin windows, finds the average of each one window. Since
    the last two windows are truncated, finds the average for two values for the second to last
    window, and just takes the raw value for the last window that only has one number."""

    tbw_list = list(three_bin_windows(bin_list))
    # return [np.average(window) for window in tbw_list]
    return [np.median(window) for window in tbw_list]

def write_csv_bins(smoothed_dict):
    """ Given a dictionary of each gene and read counts averaged by a 3 bin sliding window over a
    defined promoter region, writes the values to a csv file.  """

    with open("binned.csv", 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter='\t')
        header = ["gene"] + [str(x) for x in range(-398, 599, 4)]
        writer.writerow(header)
        for gene, read_vals in smoothed_dict.iteritems():
            row = [gene] + [str(x) for x in read_vals]
            writer.writerow(row)

if __name__ == "__main__":
    CMD_LINE = argparse.ArgumentParser(description="Generates csv files for occupancy maps")
    CMD_LINE.add_argument("-r", "--reads", required=True)
    CMD_LINE.add_argument("-j", "--pool_size", default=multiprocessing.cpu_count(), type=int)
    CMD_LINE.add_argument("-bs", "--bin_size", default=4, type=int)
    CMD_LINE.add_argument("-ws", "--window_size", default=3, type=int)

    ARGS = CMD_LINE.parse_args()

    PROMS = promoters()
    CHOPPED = chop_promoters(PROMS)
    INTERSECTED = intersect_bins(CHOPPED, ARGS.pool_size)
    GENE_COL = collect_gene_bins(INTERSECTED)
    SMOOTHED_COL = smooth_bins(GENE_COL)
    write_csv_bins(SMOOTHED_COL)
