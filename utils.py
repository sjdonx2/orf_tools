""" Simple set of utility functions
"""

import os.path

def in_dir(filepath):
    """ Gives the absolute filepath for a file in the scripts directory
    """
    directory = os.path.dirname(os.path.abspath(__file__))
    abs_filepath = os.path.join(directory, filepath)

    return abs_filepath
