### Procedure ###
1. Generate read map csv file
    $ occupancy.py -r reads.bed \# Generates binned.csv
2. Reorder csv file according to order from Pugh data (generates new\_\* file)
    $ reorder.py ordered.csv binned.csv
3. Get top 1000 genes from that order \# Generates new\_binned.csv
    $ top_1000.sh new_binned.csv
4. Visualize with Java TreeView
