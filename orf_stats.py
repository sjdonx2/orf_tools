#!/usr/bin/env python2
""" Collect statistics of each transcription factor

For each transcription factor in TF_TABLE, provide the number of genes that it
regulates, along with the number of overlaps contained in the HU_FILE and
NO_HU_FILE and write the information to a table

Table format:
TF_name, #_of_genes_regulated, #_of_HU_overlaps, #_of_noHU_overlaps
"""

import csv
import argparse
from collections import Counter
import utils

def tf_gene_counts(tf_gene_table):
    """ Returns the number of genes each transcription factor regulates,
    returning a list of tfs and # of genes regulated in alphabetical order
    """

    counts = Counter()
    with open(tf_gene_table) as table_file:
        reader = csv.reader(table_file, delimiter=';')
        for tfactor, _ in reader:
            counts[tfactor] += 1

    return sorted(counts.items())

def gene_tf_dict(tf_gene_table):
    """ Returns a dictionary, with each gene name being a key and a list of
    transcription factors that regulates the gene as values
    """
    gene_dict = {}
    with open(tf_gene_table) as table_file:
        reader = csv.reader(table_file, delimiter=';')
        for tfactor, gene in reader:
            if gene not in gene_dict:
                gene_dict[gene] = [tfactor]
            else:
                gene_dict[gene].append(tfactor)

    return gene_dict

def calc_overlaps(exp_data, gene_dict):
    """ For every gene in the w/ and w/o hydroxyurea experimental data,
    find each transcription factor that regulates, counting the number of
    genes that each transcription factor interacts with that were found in
    the experimental data.

    ie ABF1 and RGT2 regulates the YBR190W gene, and in the no_hydroxy experiment,
    YBR190W was a gene that was found. Then ABF1 and RGT2 both have 1 overlap in
    the #_of_noHU_overlaps column.  Returns the number of overlaps for each exp_data

    Since a row can consist of just a identifier or a identifier and gene name.
    Therefore, the function first checks if the lines contains an identifier or
    identifier and gene name, whether it is in the gene_dict, adding 1 to the
    number of overlaps for that particular experiment if that is the case

    Invariant: Does not account for if the regulation table keeps both the
    systematic and standard id on different lines. Example, if the gene IOC4,
    whch also has a systematic name of YMR044W, and the transcription factor
    ACA1 regulates this gene, but the regulation table lists both ACA1;IOC4
    and ACA1;YMR044W, this function won't account for that and effectively,
    double count this regulation
    """
    overlaps = Counter()
    with open(exp_data) as obs_genes:
        for line in obs_genes:
            line = line.strip().split(',')

            for gene_id in line:
                if gene_id in gene_dict:
                    for tfactor in gene_dict[gene_id]:
                        overlaps[tfactor] += 1

    return overlaps

def write_table(hu_exp, no_hu_exp, tf_counts, gene_dict):
    """ Writes each transcription factor, number of genes regulated, overlaps
    the hydroxyurea and no hydroxyurea experiments in the format described
    above
    """
    no_hu_overlaps = calc_overlaps(no_hu_exp, gene_dict)
    hu_overlaps = calc_overlaps(hu_exp, gene_dict)
    with open("tf_overlaps.csv", 'w') as csvfile:
        writer = csv.writer(csvfile)
        for tfactor, count in tf_counts:
            tfactor_no_hu_overlaps = no_hu_overlaps.get(tfactor, 0)
            tfactor_hu_overlaps = hu_overlaps.get(tfactor, 0)
            writer.writerow([tfactor, count, tfactor_hu_overlaps, tfactor_no_hu_overlaps])

if __name__ == '__main__':
    CMD_LINE = argparse.ArgumentParser(description="""
    Produces a CSV file with the number of genes regulated, overlaps in
    hydroxyurea experimental data and overlaps in no hydroxyurea experiment,
    in alphabetical order.  e.g. GCN4,2904,255,35
    """)
    CMD_LINE.add_argument("-hu", "--hydroxyurea", \
            help="CSV file of gene names from experiment w/ hydroxyurea", \
            required=True)
    CMD_LINE.add_argument("-nohu", "--no_hydroxyurea", \
            help="CSV file of gene names from experiment w/o hydroxyurea", \
            required=True)
    ARGS = CMD_LINE.parse_args()

    HU_FILE = ARGS.hydroxyurea
    NO_HU_FILE = ARGS.no_hydroxyurea
    TF_TABLE = utils.in_dir("RegulationTable.tsv")

    COUNTS = tf_gene_counts(TF_TABLE)
    GENE_TABLE = gene_tf_dict(TF_TABLE)
    write_table(HU_FILE, NO_HU_FILE, COUNTS, GENE_TABLE)
