#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Count the number of features that fufill a certain decision,
mainly tRNAs and genes
"""

import sys
from pybedtools import BedTool

def gene_trna_count(bed_file):
    """ Count the number of features that are tRNAs or genes"""
    btool = BedTool(bed_file)
    subset = btool.filter(lambda feat: feat.fields[2] == 'gene' or feat.fields[2] == 'tRNA_gene')

    print(len(subset))

if __name__ == "__main__":
    BED_FILE = sys.argv[1]
    gene_trna_count(BED_FILE)
